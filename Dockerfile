FROM wyveo/nginx-php-fpm:latest

COPY . /usr/share/nginx/html
COPY ./nginx/nginx.conf /etc/nginx/conf.d/default.conf

WORKDIR /usr/share/nginx/html

RUN ln -s public hhtml
RUN apt update; \
    apt install nano -y

EXPOSE 80