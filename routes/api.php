<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\CustomerController;
use App\Http\Controllers\Api\OrderController;
use App\Http\Controllers\Api\ProductController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/
 
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => '/v1'], function () {

    Route::controller(ProductController::class)->group(function() {
        Route::get('/products/{id}', 'show');
        Route::post('/products', 'store');
        Route::delete('/products/{id}', 'destroy');
        Route::put('/products/{id}', 'update');
        Route::get('/products', 'index');
    });

        Route::controller(OrderController::class)->group(function() {
        Route::get('/orders/{id}', 'show');
        Route::post('/orders', 'store');
        Route::delete('/orders/{id}', 'destroy');
        Route::put('/orders/{id}', 'update');
        Route::get('/orders', 'index');
    });

    Route::controller(CustomerController::class)->group(function() {
        Route::get('/customers/{id}', 'show');
        Route::post('/customers', 'store');
        Route::delete('/customers/{id?}', 'destroy');
        Route::put('/customers/{id?}', 'update');
        Route::get('/customers', 'index');
        Route::post('/login', 'login');
        Route::post('/logout', 'logout');
        Route::post('/validate-email/{email}', 'validateEmail');
    });
});
