<?php

namespace Database\Seeders;

use Ramsey\Uuid\Uuid;
use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $products = [
            [
                'name' => 'Pastel de Queijo',
                'uuid' => Uuid::uuid4(),
                'photo' => base64_encode('teste'),
                'price' => 10.45
            ],

            [
                'name' => 'Pastel de Calabresa C/ Queijo',
                'uuid' => Uuid::uuid4(),
                'photo' => base64_encode('teste'),
                'price' => 10.45
            ],

            [
                'name' => 'Pastel de Carne',
                'uuid' => Uuid::uuid4(),
                'photo' => base64_encode('teste'),
                'price' => 10.45
            ],

            [
                'name' => 'Pastel de Frango C/ Catupiry',
                'uuid' => Uuid::uuid4(),
                'photo' => base64_encode('teste'),
                'price' => 10.45
            ],
        ];

        foreach ($products as $product) {
            Product::create($product);
        }
    }
}
