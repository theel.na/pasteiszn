<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class CustomerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    { 
        $customers = [
            [
                'name' => 'Administrador',
                'password' => 'password',
                'phone' => '977191223',
                'email' => 10.45,
                'date_of_birth' => '2022-01-01',
                'address' => "Rua 01",
                'complement_of_address' => "Nenhum",
                'neighborhood' => 'Redondezas',
                'postcode' => '04929290'
            ],

            [
                'name' => 'Cliente 01',
                'password' => 'password',
                'phone' => '977191225',
                'email' => 'cliente01@hotmail.com',
                'date_of_birth' => '2022-01-01',
                'address' => "Rua 02",
                'complement_of_address' => "Nenhum",
                'neighborhood' => 'Redondezas',
                'postcode' => '04929291'
            ],

            [
                'name' => 'Cliente 02',
                'password' => 'password',
                'phone' => '977191224',
                'email' => 'cliente02@hotmail.com',
                'date_of_birth' => '2022-01-01',
                'address' => "Rua 03",
                'complement_of_address' => "Nenhum",
                'neighborhood' => 'Redondezas',
                'postcode' => '04929292'
            ],

            [
                'name' => 'Cliente 03',
                'password' => 'password',
                'phone' => '977191226',
                'email' => 'cliente03@hotmail.com',
                'date_of_birth' => '2022-01-01',
                'address' => "Rua 04",
                'complement_of_address' => "Nenhum",
                'neighborhood' => 'Redondezas',
                'postcode' => '04929293'
            ],
        ];

        foreach ($customers as $customer) {
            User::create($customer);
        }
    }
}
