<?php

namespace App\DataAccessors;

use App\Models\User;
use Illuminate\Database\Eloquent\Collection;

interface CustomerDataAccessorInterface extends DataAccessorInterface
{
    public function getById(int $id): User;
    public function getByEmail(string $email): User|null;
    public function getAllCustomers(): Collection;
    public function deleteCustomer(User $customer): bool;
    public function createCustomer(User $customer): bool;
}
