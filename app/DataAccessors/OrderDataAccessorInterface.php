<?php

namespace App\DataAccessors;

use App\Models\Order;
use Illuminate\Database\Eloquent\Collection;

interface OrderDataAccessorInterface extends DataAccessorInterface
{
    public function getOrderById(int $id): Order;
    public function getAllOrdersByCustomer(int $userId): Collection;
    public function deleteOrder(Order $order): bool;
    public function createOrder(Order $order): Order|bool;
}