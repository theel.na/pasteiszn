<?php

namespace App\DataAccessors;

use App\Models\OrderItem;
use App\DataAccessors\DataAccessorInterface;
use Illuminate\Database\Eloquent\Collection;

interface OrderItemDataAccessorInterface extends DataAccessorInterface
{
    public function getOrderItemsByOrderId(int $orderId): Collection|array;
    public function createOrderItem(OrderItem $orderItem): bool;
}
