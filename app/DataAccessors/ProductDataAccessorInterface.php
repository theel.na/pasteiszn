<?php

namespace App\DataAccessors;

use App\Models\Product;
use App\DataAccessors\DataAccessorInterface;
use Illuminate\Database\Eloquent\Collection;

interface ProductDataAccessorInterface extends DataAccessorInterface
{
    public function getById(int $id): Product;
    public function getAllProducts(): Collection|array;
    public function deleteProduct(Product $product): bool;
    public function createProduct(Product $product): bool;
}
