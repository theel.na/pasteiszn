<?php

namespace App\DataAccessors\MySQL;

use App\Models\OrderItem;
use Illuminate\Database\Eloquent\Collection;
use App\DataAccessors\OrderItemDataAccessorInterface;

class OrderItemDataAccessor implements OrderItemDataAccessorInterface
{
    public function getOrderItemsByOrderId(int $orderId): Collection
    {
        return OrderItem::where('order_id', $orderId)->get();
    }

    public function createOrderItem(OrderItem $orderItem): bool
    {
        return $orderItem->save();
    }
}