<?php

namespace App\DataAccessors\MySQL;

use App\Models\Order;
use Illuminate\Database\Eloquent\Collection;
use App\DataAccessors\OrderDataAccessorInterface;

class OrderDataAccessor implements OrderDataAccessorInterface
{
    public function createOrder(Order $order): Order 
    {
        $order->save();
    
        return $order;
    }

    public function getOrderById(int $id): Order 
    {
        return Order::where('id', $id)->firstOrFail();
    }

    public function getOrderByIdAndCustomerId($id, $userId): Order|null 
    {
        return Order::where('id', $id)->where('user_id', $userId)->firstOrFail();
    }

    public function getAllOrdersByCustomer(int $userId): Collection
    {
        return Order::where('user_id', $userId)->get();
    }

    public function deleteOrder(Order $order): bool
    {
        return $order->delete();
    }
}
