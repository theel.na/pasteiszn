<?php

namespace App\DataAccessors\MySQL;

use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use App\DataAccessors\CustomerDataAccessorInterface;

class CustomerDataAccessor implements CustomerDataAccessorInterface
{
    public function getById(int $id): User
    {
        return User::where('id', $id)->firstOrFail();
    }

    public function getByEmail(string $email): User|null
    {
        return User::where('email', $email)->withTrashed()->first();
    }

    public function getAllCustomers(): Collection
    {
        return User::all();    
    }

    public function deleteCustomer(User $customer): bool
    {
        return $customer->delete($customer);
    }

    public function createCustomer(User $customer): bool
    {
        return $customer->save();
    }
}
