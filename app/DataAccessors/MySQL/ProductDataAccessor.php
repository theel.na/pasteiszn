<?php

namespace App\DataAccessors\MySQL;

use App\DataAccessors\ProductDataAccessorInterface;
use App\Models\Product;
use Illuminate\Database\Eloquent\Collection;

class ProductDataAccessor implements ProductDataAccessorInterface
{
    public function getById(int $id): Product
    {
        return Product::where('id', $id)->firstOrFail();
    }

    public function getAllProducts(): Collection 
    {
        return Product::all();
    }
    
    public function deleteProduct(Product $product): bool
    {
        return $product->delete($product);
    }

    public function createProduct(Product $product): bool 
    {
        return $product->save();
    }
}
