<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;

use Attribute;
use Laravel\Sanctum\HasApiTokens;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'phone',
        'date_of_birth',
        'address',
        'complement_of_address',
        'neighborhood',
        'postcode',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];    
    
    public function getId(): int 
    {
        return $this->id;
    }

    public function getName(): string 
    {
        return $this->name;
    }

    public function setName(string $name): void 
    {
        $this->name = $name;
    }

    public function getEmail(): string 
    {
        return $this->email;
    }

    public function setEmail(string $email): void 
    {
        $this->email = $email;
    }

    public function getPhone(): string 
    {
        return $this->phone;
    }

    public function setPhone(string $phone): void 
    {
        $this->phone = $phone;
    }

    public function getDateOfBirth(): string 
    {
        return $this->date_of_birth;
    }

    public function setDateOfBirth(string $dateOfBirth): void 
    {
        $this->date_of_birth = $dateOfBirth;
    }

    public function getAddress(): string 
    {   
        return $this->address;
    }

    public function setAddress(string $address): void 
    {
        $this->address = $address;
    }

    public function getComplementOfAddress(): string 
    {
        return $this->complement_of_adress;
    }

    public function setComplementOfAddress(string $complementOfAddress): void 
    {
        $this->complement_of_address = $complementOfAddress;
    }

    public function getNeighborhood(): string 
    {
        return $this->postcode;
    }

    public function setNeighborhood(string $neighborhood): void 
    {
        $this->neighborhood = $neighborhood;
    }

    public function getPostcode(): string 
    {
        return $this->postcode;
    }

    public function setPostcode(string $postcode): void 
    {
        $this->postcode = $postcode;
    }

    public function getCreatedAt(): string
    {
        return $this->created_at;
    }

    public function getPassword(): string 
    {
        return $this->password;
    }

    public function setPassword(string $password): void 
    {
        $this->password = $password;
    }
    
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    /* public function isAdmin(): Attribute 
    {
        return new Attribute(
            get: fn ($value) => $value === 1 ? 'Yes' : 'No',
            set: fn ($value) => $value === 'Yes' ? 1 :0
        );
    }*/
}
