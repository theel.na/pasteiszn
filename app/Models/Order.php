<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Order extends Model
{
    use HasFactory, SoftDeletes;
   
    protected $fillable = [
        'user_id',
        'total_price',
        'status'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function getId(): int 
    {
        return $this->id;
    }

    public function orderItems(): HasMany 
    {
        return $this->hasMany(OrderItems::class);
    }

    public function user(): BelongsTo 
    {
        return $this->belongsTo(User::class);
    }

    public function getUserId(): string 
    {
        return $this->user_id;
    }

    public function setCustomerId(int $id): void 
    {
        $this->user_id = $id;
    }

    public function getTotalPrice(): float 
    {
        return $this->total_price;
    } 

    public function setTotalPrice(float $totalPrice): void 
    {
        $this->total_price = $totalPrice;
    }

    public function getStatus(): string 
    {
        return $this->status;
    }

    public function setStatus(string $status): void 
    {
        $this->status = $status;
    }
}
