<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class OrderItem extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'order_id',
        'product_id',
        'quantity',
        'price',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function order(): BelongsTo 
    {
        return $this->belongsTo(Order::class);
    }

    public function product(): belongsTo 
    {
        return $this->belongsTo(Product::class);
    }

    public function getId(): int
    {
        return $this->id();
    }

    public function getOrderId(): int 
    {
        return $this->order_id;
    }

    public function setOrderId(int $orderId): void 
    {
        $this->order_id = $orderId;
    }

    public function getQuantity(): int 
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): void 
    {
        $this->quantity = $quantity;
    }

    public function getPrice(): float 
    {
        return $this->price;
    }

    public function setPrice(float $price): void 
    {
        $this->price = $price;
    }

    public function setProductId(int $id): void 
    {
        $this->product_id = $id;
    }
}
