<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Product extends Model
{
    use HasFactory, SoftDeletes;
   
    protected $fillable = [
        'name',
        'uuid',
        'price',
        'photo',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function getId(): int 
    {
        return $this->id;
    }

    public function getName(): string 
    {
        return $this->name;
    }

    public function setName(string $name): void 
    {
        $this->name = $name;
    }

    public function getUuid(): string 
    {
        return $this->uuid;
    }

    public function setUuid(string $uuid): void 
    {
        $this->uuid = $uuid;
    }

    public function getPrice(): float 
    {
        return $this->price;
    } 

    public function setPrice(float $price): void 
    {
        $this->price = $price;
    }

    public function getPhoto(): string 
    {
        return $this->photo;
    }

    public function setPhoto(string $photo): void 
    {
        $this->photo = $photo;
    }
}
