<?php

namespace App\Factories;
use Ramsey\Uuid\Uuid;

use App\Models\Product;


class ProductFactory
{
    public function create(string $name, string $photo, float $price): Product
    {
        $product = new Product();
        $product->setUuid(Uuid::uuid4());
        $product->setName($name);
        $product->setPhoto($photo);
        $product->setPrice($price);

        return $product;
    }
}
