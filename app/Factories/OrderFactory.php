<?php

namespace App\Factories;

use App\Models\User;
use App\Models\Order;

class OrderFactory
{
    public function create(User $customer, float $totalPrice, string $status): Order
    {
        $order = new Order();
        $order->user()->associate($customer);
        $order->setTotalPrice($totalPrice);
        $order->setStatus($status);

        return $order;
    }
}
