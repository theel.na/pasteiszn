<?php

namespace App\Factories;

use App\Models\User;


class CustomerFactory
{
    public function create(
        string $name,
        string $email,
        string $password,
        string $phone = null,
        string $dateOfBirth,
        string $address,
        string $neighborhood,
        string $complementOfAddress,
        string $postcode
    ): User {
        $customer = new User();
        $customer->setName($name);
        $customer->setEmail($email);
        $customer->setPassword($password);
        $customer->setPhone($phone);
        $customer->setDateOfBirth($dateOfBirth);
        $customer->setAddress($address);
        $customer->setNeighborhood($neighborhood);
        $customer->setComplementOfAddress($complementOfAddress);
        $customer->setPostcode($postcode);

        return $customer;
    }
}
