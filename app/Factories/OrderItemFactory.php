<?php

namespace App\Factories;

use App\Models\Order;
use App\Models\Product;
use App\Models\OrderItem;

class OrderItemFactory
{
    public function create(Order $order, Product $product, float $price, int $quantity): OrderItem
    {
        $orderItem = new OrderItem();
        $orderItem->order()->associate($order);
        $orderItem->setOrderId($order->getId());
        $orderItem->product()->associate($product);
        $orderItem->setProductId($product->getId());
        $orderItem->setPrice($price);
        $orderItem->setQuantity($quantity);

        return $orderItem;
    }
}
