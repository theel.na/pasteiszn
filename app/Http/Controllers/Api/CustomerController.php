<?php

namespace App\Http\Controllers\Api;

use App\Responses\Response;
use Illuminate\Http\Request;
use App\Helpers\ControllerHelper;
use App\Services\CustomerService;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Exceptions\CustomerException;

class CustomerController extends Controller
{

    public function __construct(
        private ControllerHelper $controllerHelper,
        private CustomerService $customerService
    ) {
    }

    /**
     * Display a listing of the resource.
     */
    public function index(): JsonResponse
    {
        try {

            return $this->controllerHelper->successJsonResponse(
                Response::HTTP_OK,
                'Lista de Usuários',
                $this->customerService->getAllCustomers()
            );
        } catch (\Exception $e) {
            return $this->controllerHelper->errorJsonResponse(
                Response::HTTP_BAD_REQUEST,
                $e->getMessage()
            );
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request): JsonResponse
    {
        $request = $request->get('data');
        
        try {

            return $this->controllerHelper->successJsonResponse(
                Response::HTTP_CREATED,
                'Usuário Cadastrado com Sucesso !',
                $this->customerService->createCustomer(
                    $request['name'],
                    $request['email'],
                    $request['password'],
                    $request['phone'],
                    $request['address'],
                    $request['dateOfBirth'],
                    $request['neighborhood'],
                    $request['complementOfAddress'],
                    $request['postcode'],
                )
            );
        } catch (\Exception $e) {
            return $this->controllerHelper->errorJsonResponse(
                Response::HTTP_BAD_REQUEST,
                $e->getMessage()
            );
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(int $id): JsonResponse
    {
        try {

            return $this->controllerHelper->successJsonResponse(
                Response::HTTP_OK,
                'Usuário Disponível',
                $this->customerService->getCustomerById(Auth::user()->id)
            );
        } catch (\Exception $e) {
            return $this->controllerHelper->errorJsonResponse(
                Response::HTTP_BAD_REQUEST,
                $e->getMessage()
            );
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, ?int $id): JsonResponse
    {
        $request = $request->get('data');

        try {

            return $this->controllerHelper->successJsonResponse(
                Response::HTTP_OK,
                'Usuário Atualizado com Sucesso !',
                $this->customerService->updateCustomer(
                    $this->customerService->getCustomerById(Auth::user()->id),
                    $request
                )
            );
        } catch (\Exception $e) {
            return $this->controllerHelper->errorJsonResponse(
                Response::HTTP_BAD_REQUEST,
                $e->getMessage()
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(?int $id): JsonResponse
    {
        try {
            $customer = $this->customerService->getLoggedCustomer();

            return $this->controllerHelper->successJsonResponse(
                Response::HTTP_OK,
                'Usuário Excluido com sucesso',
                $this->customerService->deleteCustomer(
                    $customer
                )
            );
        } catch (\Exception $e) {
            return $this->controllerHelper->errorJsonResponse(
                Response::HTTP_BAD_REQUEST,
                $e->getMessage()
            );
        }
    }

    public function login(): JsonResponse 
    {
        try {
            $credentials = request(['email', 'password']);

            if (empty($credentials['email']) || empty($credentials['password'])) {
                throw CustomerException::missingFields();
            }

            $token = auth()->attempt($credentials);

            if (!$token) {
                throw CustomerException::unauthorized();
            }

            return $this->controllerHelper->successJsonResponse(
                Response::HTTP_OK,
                'Login Realizado com sucesso',
                $token
            );
        } catch (\Exception $e) {
            return $this->controllerHelper->errorJsonResponse(
                Response::HTTP_BAD_REQUEST,
                $e->getMessage()
            );
        }
    }

    public function logout(): JsonResponse
    {
        try {
            auth()->invalidate();
            
            return $this->controllerHelper ->successJsonResponse(
                Response::HTTP_OK,
                'Logout Realizado com sucesso',
            );
        } catch (\Exception $e) {
            return $this->controllerHelper->errorJsonResponse(
                Response::HTTP_BAD_REQUEST,
                $e->getMessage()
            );
        }
    }

    public function validateEmail(string $email): JsonResponse
    {
        try {
            $customer = $this->customerService->getLoggedCustomer();

            if ($customer->hasVerifiedEmail()) {
                throw CustomerException::customerHasVerifiedEmail();
            }

            if (!$this->customerService->setEmailVerified($customer)) {
                throw new \Exception('Ocorreu um erro ao verificar o e-mail informado.');
            }

            
            return $this->controllerHelper ->successJsonResponse(
                Response::HTTP_OK,
                'Email validado com sucesso',
            );
        } catch (\Exception $e) {
            return $this->controllerHelper->errorJsonResponse(
                Response::HTTP_BAD_REQUEST,
                $e->getMessage()
            );
        }
    }
}
