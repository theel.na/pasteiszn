<?php

namespace App\Http\Controllers\Api;

use App\Responses\Response;
use Illuminate\Http\Request;
use App\Services\ProductService;
use App\Helpers\ControllerHelper;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{

    public function __construct(
        private ControllerHelper $controllerHelper,
        private ProductService $productService
    ) {
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try {

            return $this->controllerHelper->successJsonResponse(
                Response::HTTP_OK,
                'Lista de Produtos',
                $this->productService->getAllProducts()
            );
        } catch (\Exception $e) {
            return $this->controllerHelper->errorJsonResponse(
                Response::HTTP_BAD_REQUEST,
                $e->getMessage()
            );
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request = $request->get('data');

        try {

            return $this->controllerHelper->successJsonResponse(
                Response::HTTP_CREATED,
                'Produto Inserido com Sucesso !',
                $this->productService->createProduct(
                    $request['name'],
                    $request['photo'],
                    $request['price']
                )
            );
        } catch (\Exception $e) {
            return $this->controllerHelper->errorJsonResponse(
                Response::HTTP_BAD_REQUEST,
                $e->getMessage()
            );
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        try {

            return $this->controllerHelper->successJsonResponse(
                Response::HTTP_OK,
                'Produto Disponível',
                $this->productService->getProductById($id)
            );
        } catch (\Exception $e) {
            return $this->controllerHelper->errorJsonResponse(
                Response::HTTP_BAD_REQUEST,
                $e->getMessage()
            );
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request = $request->get('data');
        
        try {

            return $this->controllerHelper->successJsonResponse(
                Response::HTTP_OK,
                'Produto Atualizado com sucesso',
                $this->productService->updateProduct(
                    $this->productService->getProductById($id),
                    $request
                )
            );
        } catch (\Exception $e) {
            return $this->controllerHelper->errorJsonResponse(
                Response::HTTP_BAD_REQUEST,
                $e->getMessage()
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {

            return $this->controllerHelper->successJsonResponse(
                Response::HTTP_OK,
                'Produto Excluido com sucesso',
                $this->productService->deleteProduct($this->productService->getProductById($id))
            );
        } catch (\Exception $e) {
            return $this->controllerHelper->errorJsonResponse(
                Response::HTTP_BAD_REQUEST,
                $e->getMessage()
            );
        }
    }
}
