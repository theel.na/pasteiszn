<?php

namespace App\Http\Controllers\Api;

    use App\Responses\Response;
use Illuminate\Http\Request;
use App\Services\OrderService;
use App\Services\ProductService;
use App\Helpers\ControllerHelper;
use App\Services\CustomerService;
use App\Services\OrderItemService;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class OrderController extends Controller
{
    public function __construct(
        private ControllerHelper $controllerHelper,
        private OrderService $orderService,
        private CustomerService $customerService,
        private OrderItemService $orderItemService,
        private ProductService $productService
    ) {
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try {

            return $this->controllerHelper->successJsonResponse(
                Response::HTTP_OK,
                'Lista de Pedidos',
                $this->orderService->getAllOrdersByCustomer(Auth::user()->id)
            );
        } catch (\Exception $e) {
            return $this->controllerHelper->errorJsonResponse(
                Response::HTTP_BAD_REQUEST,
                $e->getMessage()
            );
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request = $request->get('data');
        
        try {
            $customer = $this->customerService->getCustomerById(Auth::user()->id);
            $order = $this->orderService->createOrder($customer);

            $orderItemsPrice = array_map(function ($product, $quantity) use ($order) {
                return $this->orderItemService->createOrderItemAndReturnTotalPrice($order, $this->productService->getProductById($product), $quantity);
            }, $request['product'], $request['quantity']);

            $order = $this->orderService->updateOrder($order, ['total_price' => round(array_sum($orderItemsPrice), 1)]);
            
            Mail::to($customer->getEmail());
            
            return $this->controllerHelper->successJsonResponse(
                Response::HTTP_CREATED,
                'Pedido Criado com sucesso',
                [
                    'order' => $order,
                    'order_items' => $this->orderItemService->getOrderItemsByOrderId($order->getId())
                ]
            );
        } catch (\Exception $e) {
            return $this->controllerHelper->errorJsonResponse(
                Response::HTTP_BAD_REQUEST,
                $e->getMessage()
            );
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        try {
            return $this->controllerHelper->successJsonResponse(
                Response::HTTP_OK,
                'Pedido',
                $this->orderService->getOrderById($id)
            );
        } catch (\Exception $e) {
            return $this->controllerHelper->errorJsonResponse(
                Response::HTTP_BAD_REQUEST,
                $e->getMessage()
            );
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        try {

            return $this->controllerHelper->successJsonResponse(
                Response::HTTP_OK,
                'Pedido Atualizado Com Sucesso',
                #$this->orderService->updateOrder()
            );
        } catch (\Exception $e) {
            return $this->controllerHelper->errorJsonResponse(
                Response::HTTP_BAD_REQUEST,
                $e->getMessage()
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {

            return $this->controllerHelper->successJsonResponse(
                Response::HTTP_OK,
                'Pedido excluido com sucesso',
               # $this->orderService->deleteOrder()
            );
        } catch (\Exception $e) {
            return $this->controllerHelper->errorJsonResponse(
                Response::HTTP_BAD_REQUEST,
                $e->getMessage()
            );
        }
    }
}
