<?php

namespace App\Services;
use App\Models\User;

use App\Factories\CustomerFactory;
use Illuminate\Support\Facades\Auth;
use App\Exceptions\CustomerException;
use Illuminate\Database\Eloquent\Collection;
use App\DataAccessors\CustomerDataAccessorInterface;

class CustomerService 
{
    public function __construct(
        private CustomerDataAccessorInterface $customerDataAccessor,
        private CustomerFactory $customerFactory,
    ) {
    }
    
    public function getCustomerById(int $id): User
    {
        if ($id !== Auth::user()->id) {
            throw CustomerException::differentCustomerRequestingData();
        }

        return $this->customerDataAccessor->getById($id);
    } 

    public function getCustomerByEmail(string $email): User 
    {
        return $this->customerDataAccessor->getByEmail($email);
    }

    public function getAllCustomers(): Collection 
    {
        return $this->customerDataAccessor->getAllCustomers();
    }

    public function deleteCustomer(User $customer): bool
    {
        return $this->customerDataAccessor->deleteCustomer($customer);
    }

    public function createCustomer(
        string $name,
        string $email,
        string $password,
        string $phone,
        string $dateOfBirth,
        string $address,
        string $neighborhood,
        string $complementOfAddress,
        string $postcode,
    ): bool {
        
        if ($this->customerDataAccessor->getByEmail($email)) {
            throw CustomerException::emailAlreadyRegistered();
        }

        $customer = $this->customerFactory->create(
            $name,
            $email,
            $password,
            $phone,
            $dateOfBirth,
            $address,
            $neighborhood,
            $complementOfAddress,
            $postcode
        );

        return $this->customerDataAccessor->createCustomer($customer);
    }

    public function updateCustomer(User $customer, array $data) 
    {
        $customer = $customer->fill($data);
        $this->customerDataAccessor->createCustomer($customer);
        
        return $customer;
    }

    public function getLoggedCustomer(): User
    {
        try {
            if (empty(auth()->user()->id)) {
                throw CustomerException::differentCustomerRequestingData();
            }

            return $this->customerDataAccessor->getById(auth()->user()->id);
        } catch (\Exception $e) {
            throw new \Exception('Usuário não é válido ou não está autenticado');
        }
    }

    public function setEmailVerified(User $user): bool 
    {
        return $user->markEmailAsVerified();
    }
}