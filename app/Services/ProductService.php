<?php

namespace App\Services;
use App\Models\Product;

use App\Factories\ProductFactory;
use Illuminate\Database\Eloquent\Collection;
use App\DataAccessors\ProductDataAccessorInterface;

class ProductService 
{
    public function __construct(
        private ProductDataAccessorInterface $productDataAccessor,
        private ProductFactory $productFactory,
    ) {
    }
    
    public function getProductById(int $id): Product
    {
        return $this->productDataAccessor->getById($id);
    } 

    public function getAllProducts(): Collection 
    {
        return $this->productDataAccessor->getAllProducts();
    }

    public function deleteProduct(Product $product): bool
    {
        return $this->productDataAccessor->deleteProduct($product);
    }

    public function createProduct(string $name, string $photo, float $price): bool 
    {
        $product = $this->productFactory->create($name, $photo, $price);

        return $this->productDataAccessor->createProduct($product);
    }

    public function updateProduct(Product $product, array $data) 
    {
        $product = $product->fill($data);
        $this->productDataAccessor->createProduct($product);
        
        return $product;
    }
}