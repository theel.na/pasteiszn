<?php

namespace App\Services;

use App\Models\Order;
use App\Models\Product;
use App\Models\OrderItem;
use App\Factories\OrderItemFactory;
use Illuminate\Database\Eloquent\Collection;
use App\DataAccessors\MySQL\OrderItemDataAccessor;

class OrderItemService 
{
    public function __construct(
        private OrderItemDataAccessor $orderItemDataAccessor,
        private OrderItemFactory $orderItemFactory,
        private OrderService $orderService,
    ) {
    }
    
    public function getOrderItemsByOrderId(int $orderId): Collection
    {
        return $this->orderItemDataAccessor->getOrderItemsByOrderId($orderId);
    }

    public function createOrderItemAndReturnTotalPrice(Order $order, Product $product, int $quantity)
    {
        $totalPrice = [];

        $orderItemPrice = self::calculateOrderItemPrice($product, $quantity);
        
        $orderItem = $this->orderItemFactory->create($order, $product, $orderItemPrice, $quantity);

        $this->orderItemDataAccessor->createOrderItem($orderItem);

        return $totalPrice[] = $orderItemPrice;
    }

    private function calculateOrderItemPrice(Product $product, int $quantity) 
    {
        return $product->getPrice() * $quantity;
    }
}