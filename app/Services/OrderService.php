<?php

namespace App\Services;

use App\Models\User;
use App\Models\Order;
use App\Factories\OrderFactory;
use Illuminate\Database\Eloquent\Collection;
use App\DataAccessors\MySQL\OrderDataAccessor;

class OrderService 
{

    PUBLIC CONST ORDER_STATUS_IN_PREPARE = 'order_in_prepare';  
    PUBLIC CONST DEFAULT_PRICE = 0.0;
    
    public function __construct(
        private OrderDataAccessor $orderDataAccessor,
        private OrderFactory $orderFactory
    ) {
    }
    
    public function getOrderById(int $orderId): Order
    {
        return $this->orderDataAccessor->getOrderById($orderId);
    } 

    public function getOrderByIdAndCustomerId(int $orderId, int $customerId): Order|null 
    {
        return $this->orderDataAccessor->getOrderByIdAndCustomerId($orderId, $customerId);
    }

    public function createOrder(User $user, ?float $totalPrice = null, ?string $status = null): Order|bool
    {
        $order = $this->orderFactory->create($user, self::DEFAULT_PRICE, self::ORDER_STATUS_IN_PREPARE);

        return $this->orderDataAccessor->createOrder($order);
    }

    public function updateOrder(Order $order, array $data) 
    {
        $order = $order->fill($data);
        $this->orderDataAccessor->createOrder($order);
        
        return $order;
    }

    public function getAllOrdersByCustomer(int $customerId): Collection 
    {
        return $this->orderDataAccessor->getAllOrdersByCustomer($customerId);
    }

    public function deleteOrder(Order $order) 
    {
        return $this->orderDataAccessor->deleteOrder($order);
    }
}