<?php

namespace App\Exceptions;

class CustomerException extends \Exception
{
    public static function emailAlreadyRegistered(): self
    {
        return new self('Email já cadastrado !');
    }

    public static function differentCustomerRequestingData(): self
    {
        return new self('Usuário Inválido !');
    }

    public static function missingFields(): self 
    {
        return new self('É necessário login e senha para realizar o login !');
    }

    public static function unauthorized(): self 
    {
        return new self('Login ou senha incorretos !');
    }
    public static function customerHasVerifiedEmail(): self 
    {
        return new self('Usuario já possui email verificado');
    }
}
