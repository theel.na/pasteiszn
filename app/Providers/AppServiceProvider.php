<?php

namespace App\Providers;

use App\DataAccessors;
use App\DataAccessors\MySQL\OrderDataAccessor;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(DataAccessors\ProductDataAccessorInterface::class, function () {
            return new DataAccessors\MySQL\ProductDataAccessor();
        });

        $this->app->bind(DataAccessors\CustomerDataAccessorInterface::class, function () {
            return new DataAccessors\MySQL\CustomerDataAccessor();
        });

        $this->app->bind(DataAccessors\OrderDataAccessorInterface::class, function () {
            return new DataAccessors\MySQL\OrderDataAccessor();
        });

        $this->app->bind(DataAccessors\OrderItemDataAccessorInterface::class, function() {
            return new DataAccessors\MySQL\OrderItemDataAccessor();
        });
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
    
    }
}
