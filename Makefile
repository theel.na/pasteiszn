.ONESHELL:
export SHELL := /bin/bash
export SHELLOPTS := $(if $(SHELLOPTS),$(SHELLOPTS):)errexit:pipefail
export DOCKER_BUILDKIT=1
export COMPOSE_DOCKER_CLI_BUILD=1

clean:
	docker-compose -p pasteiszn_application down --rmi local --volumes

setup-local:
	docker-compose -p pasteiszn_application up -d
	docker exec -it pasteiszn_application_application_1 php artisan cache:clear && \
				 	php artisan view:clear && \
				 	php artisan route:clear && \
				 	php artisan config:clear && \
				 	php artisan key:generate && \
				 	php artisan storage:link && \
					php artisan jwt:secret --always-no && \
				 	composer install
local-serve:
	docker-compose -p pasteiszn_application up

populate-db:
	docker exec -it pasteiszn_application_application_1 php artisan migrate --seed 
					

