# Pastéis ZN

## Descrição do Projeto
<p align="center">Api criada para o Desafio Backend da Commerc</p>

### Pré-requisitos

Antes de começar, você vai precisar ter instalado em sua máquina as seguintes ferramentas:
[Git](https://git-scm.com), [Docker](https://www.docker.com/), [Docker Compose](https://docs.docker.com/compose/), [Make](https://howtoinstall.co/pt/make), 
Além disto é bom ter um editor para trabalhar com o código como [VSCode](https://code.visualstudio.com/)

### 🎲 Rodando o Back End (servidor)

```bash
# Clone este repositório
$ git clone git@gitlab.com:theel.na/pasteiszn.git

# Acesse a pasta do projeto no terminal/cmd
$ cd pasteiszn

# Rode o seguintes comandos
$ make setup-local
$ make local-serve
$ make populate-db

# O servidor inciará na porta:9011 - acesse <http://localhost:9011>
```